import fetch_helper
import time
import re
from concurrent.futures import ThreadPoolExecutor, as_completed
from sup_helpers import createDirsIfNeeded, get_progress_str, utils, Diff, print_, DEBUG, WHITELIST
import os
from typing import List

# Returns the saved diff path
def fetch_and_save_diff(diff: Diff, root_dir: str, use_subdirs: bool) -> Diff:
    patch_path = createDirsIfNeeded(f'{root_dir}/{diff.patch}') if use_subdirs else root_dir
    return fetch_helper.download_diff(diff, patch_path)


def fetch_untrheaded(util: str, download_dir: str, use_subdirs: bool):
    patches = fetch_helper.getPatchUrls(util)
    root_dir = createDirsIfNeeded(download_dir)

    paths = []

    for patch_name, patch_url in patches.items():
        paths.append(fetch_and_save_diff(patch_url, root_dir, patch_name))

    return paths

def diff_blacklist(diff_url: str) -> bool:
    diff = diff_url.split("/")[-1]
    # I'm lazy. Most of these diffs just have a malformed name
    # and will cause the parsing process to go belly up
    blacklist = [
            "dwm-anybar-v1.0.3-to-v1.1.0.diff",
            "dwm-r1437-gaplessgrid.diff",
            "dwm-5.8-gestures.diff",
            "dwm-5.2-gaplessgrid.diff",
            "dwm-killunsel-ceac8c91ff.diff"
            "dwm-ipc-v1.5.6-to-v1.5.7.diff",
            "dwm-r1437-moveontagmon.diff",
            "dwm-r1615-mpdcontrol.diff",
            "dwm-git-20120406-pertag.diff",
            "dwm-r1578-pertag.diff",
            "dwm-6.1-pertag_without_bar.diff",
            "dwm-6.0-pertag_without_bar.diff",
            "dwm-5.8.2-pertag_without_bar.diff",
            "dwm-script_tags-without_fifo.diff",
            "dwm-r1615-selfrestart.diff",
            "dwm-6.1-single_tagset.diff",
            "dwm-6.0-spawn_cwd.diff",
            "dwm-6.0-singularborders.diff",
            "dwm-6.0-singularborders_bstack.diff",
            "dwm-6.0-smfact.diff",
            "dwm-5.7.2-statuscolors.diff",
            "dwmblocks-statuscmd.diff",
            "dwm-6.1-statusallmons.diff",
            "dwm-5.8.2-statuscolors.diff",
            "dwm-r1533-stdin.diff",
            "dwm-5.8.2-swap.diff",
            "dwm-6.1-tab-v2b.diff",
            "dwm-6.1-systray.diff",
            "dwm-git-20130119-systray.diff",
            "dwm-6.1-taggrid.diff",
            "dwm-tab-v2b-56a31dc.diff",
            "dwm-6.2-tab-v2b.diff",
            "dwm-6.2-taggrid.diff",
            "dwm-6.1-tagintostack-allmaster.diff",
            "dwm-6.1-taggrid.diff",
            "dwm-6.2-tagintostack-onemaster.diff",
            "dwm-6.2-urg-border.diff",
            "dwm-6.1-min-border.diff",
            "dwm-6.1-urg-border.diff",
            "dwm-cfacts-vanitygaps-6.2_combo.diff",
            "dwm-r1522-viewontag.diff",
            "dwm-6.1-xkb.diff",
            "dwm-6.0-xtile.diff",
            "dwm-6.1-pertag-tab-v2b.diff",
            "dwm-tab-v2b-pertab-56a31dc.diff",
            "dwm-6.0-winview.diff",
            "attachabove-6.2-20200421.diff", # references to files are wonky
            "st-focus-20200731-patch_alpha.diff",
            "st-disable-bold-italic-fonts.diff",
            "st-ligatures-20200430-0.8.3.diff",
            "st-ligatures-scrollback-20200430-0.8.3.diff",
            "st-ligatures-alpha-20200430-0.8.3.diff",
            "st-ligatures-alpha-scrollback-20200430-0.8.3.diff",
            "netwmicon.sh",
            "st-openclipboard-20190202-0.8.1.diff",
            "plumb_without_shell_OSC.diff",
            "simple_plumb.diff",
            "st-ligatures-boxdraw-20200430-0.8.3.diff",
            "right_click_to_plumb.diff",
            "st-xresources-20181018-g30ec9a3.diff", #bad hash _G_30ec9a3
            ]

    debug_whitelist = []

    if DEBUG() and WHITELIST():
        return diff in str(debug_whitelist)

    return not diff in str(blacklist)
    #return not any(blacklisted in diff_url for blacklisted in blacklist)

def fetch(util: str, download_dir: str, use_subdirs: bool) -> List[Diff]:
    patches = fetch_helper.getPatchUrls(util)

    diffs = []
    with ThreadPoolExecutor() as pool:
        results = [pool.submit(fetch_helper.getDiffUrls, p_url) for p_url in patches.values()]
        for res in as_completed(results):
                diffs.extend(map(lambda url: Diff(url), filter(diff_blacklist,res.result())))

    root_dir = createDirsIfNeeded(download_dir)

    with ThreadPoolExecutor() as pool:
        results = [pool.submit(fetch_and_save_diff, diff, root_dir, use_subdirs) for diff in diffs]

        total = len(results)
        print_(f'Downloading {total} patches for {util}')
        cur = 0
        paths = []
        for result in as_completed(results):
            cur += 1
            paths.append(result.result())
            print_(get_progress_str(cur, total), end='' if cur != total else '\n')

    return paths

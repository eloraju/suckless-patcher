#!/usr/bin/python
import os
import git
import re
from sup_helpers import Diff, print_debug

from shutil import copyfile

def patch(repo: git.Repo, diff: Diff) -> (str, bool):
    patch_branch = f"patch/{diff.filename}"
    # Parse the diff file for metadata and checkout the
    # parent branch to prevent conflicts during patching
    root_branch = repo.active_branch.name
    try:
        repo.git.checkout(diff.parent)
    except:
        print_debug(f"Invalid parent {diff.parent}. Trying to apply to current branch")
        

    repo.create_head(patch_branch).checkout()
    print_debug(f"patching diff:\n{str(diff)}")

    ok = False
    diff_path = os.path.abspath(diff.path)
    try:
        repo.git.am(diff_path)
        repo.git.checkout(root_branch)
        return (diff.filename, True)
    except Exception as err:
        if re.search("am session", repo.git.status()):
            repo.git.am("--abort")

        repo.git.reset("--hard")
        repo.git.clean("-f")
        print_debug(f"\n\nError with {diff.filename} AM")
        print_debug(err)
        print_debug(f"Error with {diff.filename}\n\n")

    try:
        repo.git.apply(diff_path)
        ok = True
    except Exception as err:
        print_debug(f"\n\nError with {diff.filename} APPLY")
        print_debug(err)
        print_debug(f"Error with {diff.filename}\n\n")
        repo.git.reset("--hard")
        repo.git.clean("-f")

    if not ok:
        try:
            res = os.system(f"(cd {repo.git.rev_parse('--show-toplevel')} && patch -p1 1>/dev/null 2>&1) < {diff_path}")
            if res != 0:
                raise Exception("Something bad happened")
        except Exception as err:
            print_debug(f"\n\nError with {diff.filename} PATCH")
            print_debug(err)
            print_debug(f"Error with {diff.filename}\n\n")
            rollback(repo, patch_branch, root_branch)
            return (diff.filename, False)


    repo.git.add("-A")
    repo.git.commit("-am", f"Applied patch {diff.filename}")
    repo.git.checkout(root_branch)
    return (diff.filename, True)

def rollback(repo: git.Repo, patch_branch: str, root_branch: str):
    # Rollback and create a new branch for the failed patch
        # Copy the diff-file and commit the change
        # This way it's just that bit easier to apply the patch manually
        repo.git.reset("--hard")
        repo.git.clean("-f")
        repo.git.checkout(root_branch)
        repo.git.branch("-D", patch_branch)



import git
import math
import os
import re
from shutil import rmtree
from typing import List

class SucklessUtil:
    name = "suckless"
    isTool = False

    def __init__(self, name: str, isTool: bool = False):
        self.name = name
        self.isTool = isTool

    def url(self):
        if self.isTool:
            return f'https://tools.suckless.org/{self.name}/'
        else:
            return f'https://{self.name}.suckless.org/'

    def patches(self):
        return self.url()+"patches/"

    def repo(self):
        return f'https://git.suckless.org/{self.name}'


class Diff:
    def __init__(self, diff_url: str = None, diff_path: str = None):
        if diff_path:
            match = re.search(r"(?P<parent>(?:[0-9.]+|[a-f0-9]{6,})).diff", diff_path)
            self.filename = diff_path.split("/")[-1].replace(".diff", "")
            self.parent = match.groups("parent")
            self.path = diff_path
            self.url = None
            self.patch = None
            self.util = None
            return

        matches = re.search(r"https?://(?P<altname>[\w]+)[.\w]+/(?:(?!patches)(?P<tool>[\w]+)|patches)(?:(?!/[/\w]+\.diff)[\w/-]+)/(?:(?P<util>[\w]+)-|(?:patches)|)(?P<name>(?P<patch>[a-z0-9+_-]+)-(?:(?P<date>[\d]{6,}|\d+-\d+-\d+)-(?P<commit>[0-9a-f]+)|(?P<release>[\dr.-]+)))\.diff", diff_url, re.IGNORECASE)

        print_debug(f"Diff.__init__(): Initiating Diff\n{diff_url}")

        self.util =  matches.group("tool") if matches.group("altname") == "tools" else matches.group("altname")
        self.patch = matches.group("patch")
        self.filename = matches.group("name")
        self.parent = matches.group("commit") if matches.group("commit") else matches.group("release")
        self.url = diff_url
        self.path = None

    def set_path(self, diff_path: str):
        self.path = diff_path

    def __str__(self) -> str:
        return f"util: {self.util}\n\tpatch: {self.patch}\n\tparent: {self.parent}\n\tfilename: {self.filename}\n\tpath: {self.path}\n\turl: {self.url}"


utils = {
    'dwm': SucklessUtil('dwm'),
    'st': SucklessUtil('st'),
    'surf': SucklessUtil('surf'),
    'dmenu': SucklessUtil('dmenu', True),
    'ii': SucklessUtil('ii', True),
    'quark': SucklessUtil('quark', True),
    'sent': SucklessUtil('sent', True),
    'sic': SucklessUtil('sic', True),
    'slock': SucklessUtil('slock', True),
    'tabbed': SucklessUtil('tabbed', True),
}

def createDirsIfNeeded(dirPath, overwrite=False) -> str:
    if os.path.exists(dirPath):
        if overwrite:
            rmtree(dirPath)
        elif not overwrite:
            return dirPath

    os.makedirs(dirPath)
    return dirPath


def path_is_dir(path) -> bool:
    return os.path.exists(path) and os.path.isdir(path)


def path_contains_repo(util: str, path: str) -> bool:
    if not path_is_dir(path):
        return False
    try:
        testR = git.Repo(path)
        for remote in testR.remotes:
            print_debug(f"path_contains_repo(): {util} in {remote.url}")
            if util in remote.url:
                return True
        print("Provided repo doesn't seem to contain the util you are trying to patch")
        return False
    except:
        return False

def existing_diffs(path: str) -> List[str]:
    diffs = []
    patches_path = createDirsIfNeeded(path)
    content = os.listdir(patches_path)

    print_debug(f"existing_diffs(): Looking into {patches_path}\n {content}")

    for folder in filter(lambda p: os.path.isdir(p), content):
        diffs.extend(existing_diffs(f"{patches_path}/{folder}"))

    diffs.extend(
            map(lambda dif: Diff(diff_path=f"{patches_path}/{dif}"),
                filter(lambda f: f"{patches_path}/{os.path.isfile(f)}" and ".diff" in f, content)))

    if DEBUG():
        print_debug(f"existing_diffs(): returning {diffs}")

    return diffs

def branchExists(branchName, repo) -> bool:
    for b in repo.branches:
        if str(b) == branchName:
            return True
    return False


def get_progress_str(cur, total):
    charCount = 25
    equalCount = math.floor(charCount * (cur / total))
    return f'\r|{equalCount*"="}>{(charCount-equalCount)*"-"}| {math.floor((cur/total)*100)}%'

def print_(msg: str, end:str ="\n"):
    if not os.environ.get("sup_silent"):
        print(msg,end=end)

def print_debug(msg):
    if DEBUG():
        print(msg, end="\n\n")

def DEBUG() -> bool:
    return bool(os.environ.get("DEBUG"))

def WHITELIST() -> bool:
    return bool(os.environ.get("WHITELIST"))

def latest_version() -> float:
    return os.environ.get("sup_latets")

def latest_commit() -> str:
    return os.environ.get("sup_latets_commit")

#!/usr/bin/python
import re
import requests

from sup_helpers import utils, SucklessUtil, Diff
from bs4 import BeautifulSoup as BS
from multiprocessing import Pool, cpu_count
from typing import List


def getPageAsSoup(url: str):
    page = requests.get(url)
    return BS(page.content, "html.parser")

def getPatchUrls(util: str) -> dict: 
    regex = re.compile(r"/patches/[\w-]+/$")
    soup = getPageAsSoup(utils[util].patches())
    found = soup.find_all(name="a", href=regex)
    patchDict = {}
    for tag in found:
        patchName = tag.get("href").split("/")[-2]
        patchDict[patchName] = "https:"+tag.get("href")
    return patchDict

def getDiffUrls(url: str) -> List[str]:
    regex = re.compile(".diff$")
    soup = getPageAsSoup(url)
    found = soup.find_all(name="a", href=regex)
    return map(lambda tag: url+tag.get("href"),
                filter(lambda tag: not re.search("github.com", tag.get("href")),found)
                )

def getDiffData(diffUrl: str) -> str:
    return requests.get(diffUrl).text

def saveDiffToFile(dest: str, data: str) -> bool:
    try:
        with open(dest, "w") as file:
            file.write(data)
        return True
    except Exception as er:
        print(er)
        return False

def download_diff(diff: Diff, dest_dir: str) -> Diff:
    diff_data = getDiffData(diff.url)
    save_path = f"{dest_dir}/{diff.filename}.diff"
    if saveDiffToFile(save_path, diff_data):
        diff.set_path(save_path)
        return diff
    else:
        raise Exception(f"Invalid diff data: \n\turl: {diff.url}\n\tdest_dir: {dest_dir}")


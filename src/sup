#!/usr/bin/python
import click
import git

import sup_helpers as sup
from patcher import patch
from fetcher import fetch, fetch_untrheaded
import fetch_helper
import os

@click.command(context_settings=dict(ignore_unknown_options=True))
@click.argument("utility",
                required=True,
                type=click.types.STRING
                )
@click.option("--repo_dir", "-r",
              type=click.types.Path(),
              help="Define the destination directory for the repo. If the path contains a repo, cloning will be skipped and the existing repo will be used in the patchigg process. The script checks remtoes for git.suckless.org/<util> to determine if the provided branch contains a repo for the probvided utility.\nDefault: <util_name>_repo",
              show_default=True
              )
@click.option("--diff-dir", "-p",
              default=None,
              type=click.types.Path(),
              help="Path to save the diffs in. If the directory exists and contains .diff files the script will skip downloading patches and uses the ones found in the provided path. Default: <util_name>_patches",
              show_default=True
              )
@click.option("--create-patch-dirs", "-d",
              default=False,
              is_flag=True,
              help="If set the script will create a folder for every patch. This options does nothing if the provided patch-dir contains diffs.",
              )
@click.option("--use-latest", "-l",
              default=False,
              is_flag=True,
              help="Tell the script to only use the latest diffs when patching",
              )
@click.option("--download-only",
              default=False,
              is_flag=True,
              help="Don't try to apply the pathces. Just download them.",
              )
@click.option("--list-patches",
              default=False,
              is_flag=True,
              help="Just fetch patch names for given utility",
              )
@click.option("--silent","-s",
              default=False,
              is_flag=True,
              help="Don't log anything",
              )
def main(utility, repo_dir, diff_dir, create_patch_dirs=False, skip_old_versions=False, download_only=False, list_patches=False, silent=False, use_latest = False):

    util = sup.utils.get(utility)
    repo_dir = os.path.abspath(repo_dir) if repo_dir else None
    patch_dir = os.path.abspath(diff_dir) if diff_dir else None

    if list_patches:
        for patch_name in fetch_helper.getPatchUrls(util.name).keys():
            sup.print_(patch_name)
        exit(0)

    if not repo_dir:
        repo_dir = f'{util.name}_repo'

    if not patch_dir:
        patch_dir = f'{util.name}_patches'

    if sup.path_contains_repo(util.name, repo_dir):
        repo = git.Repo(repo_dir)
    else:
        sup.print_(f'Cloning {util.name} from suckless.org into "{repo_dir}"')
        repo = git.Repo.clone_from(util.repo(), repo_dir)

    if not sup.path_contains_repo(util.name, repo_dir):
        sup.print_(
            f'Something went wrong with the repo initializtion. {repo_dir} does not contain a repo.')
        exit(2)

#    os.environ["sup_latets_version"] = repo.tags[-1].name
#    os.environ["sup_latets_commit"] = str(repo.head.commit)
#    os.environ["sup_latets_commit_date"] = repo.head.commit.committed_datetime.split(" ")[0]

    existing_diffs = sup.existing_diffs(patch_dir)
    patches = fetch(util.name, patch_dir, False) if not existing_diffs else existing_diffs

    if not download_only:
        cur = 0
        failed = 0
        total = len(patches)
        sup.print_("Applying patches")
        failed_patches = []
        for diff in patches:
            # here we try to apply
            patch_result = patch(repo, diff) # (str, bool)
            if not patch_result[1]:
                failed += 1
                failed_patches.append(patch_result[0])
            cur += 1
            sup.print_(sup.get_progress_str(cur, total), end='' if cur != total else '\n')
            
        sup.print_(f"Done!")
        if failed != 0:
            sup.print_(f"Failed patches ({failed}/{total}):")
            for p in failed_patches:
                sup.print_(f"    {p}")

        exit(0)

    # We get here if download_only is set so we can safely remove the repo


if __name__ == "__main__":
    # pylint: disable=no-value-for-parameter
    main()

### Suckless utility patcher

Fetch and apply patches to your favourite suckless utils

This tool will make it easier to apply patches to suckless software.
It clones the latest version of a provided suckless utility, downloads
all patches available for that utility and tries to apply every patch
against a clean branch.

You can then just merge all the patches that you want and be on your merry way.
You'll get to resolve all conflicts though...

#### Installation

Just clone the repo and run e.g. `./sup dwm` in the repo directory to create two
directories `dwm_repo/` and `dwm_patches/`. The former will contain a modified `dwm`
repository that has branches for every succesfully applied patch. You will have to
merge the features you want by hand.

#### Usage

```shell
Usage: sup [OPTIONS] UTILITY

Options:
  -r, --repo_dir PATH      Define the destination directory for the repo. If
                           the path contains a repo, cloning will be skipped
                           and the existing repo will be used in the patchigg
                           process. The script checks remtoes for
                           git.suckless.org/<util> to determine if the
                           provided branch contains a repo for the probvided
                           utility. Default: <util_name>_repo

  -p, --diff-dir PATH      Path to save the diffs in. If the directory exists
                           and contains .diff files the script will skip
                           downloading patches and uses the ones found in the
                           provided path. Default: <util_name>_patches

  -d, --create-patch-dirs  If set the script will create a folder for every
                           patch. This options does nothing if the provided
                           patch-dir contains diffs.

  -l, --use-latest         Tell the script to only use the latest diffs when
                           patching

  --download-only          Don't try to apply the pathces. Just download them.
  --list-patches           Just fetch patch names for given utility
  -s, --silent             Don't log anything
  --help                   Show this message and exit.
```

#### TODO

- [x] Create a readme
- [x] Have a working version out in the open
- [x] Try to automatically ~~merge~~ apply the patches to their own brances
- [x] Parallelize the patch fetching process
- [x] Allow usage of pre-existing repo or patches dir
  - [x] Pre existing repo
  - [x] Pre existing patches
- [ ] Create an option to make the program only apply latest patches and skip
  older versions
- [ ] Add possibility to pick which patches/diffs get downloaded
- [ ] Create repositories for versios of suckless utils that have all the
patches applied as branches.
- [ ] Create a pipeline that updates said repositories when this repo is
  updated
- [ ] Create a downloadable binary / AUR package

#### License

[MIT](https://gitlab.com/eloraju/suckless-patcher/-/blob/master/LICENSE)
